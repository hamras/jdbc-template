package com.jdbctemplate.jdbctemplate.controller;


import com.jdbctemplate.jdbctemplate.dao.EmployeeDao;
import com.jdbctemplate.jdbctemplate.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class EmployeeController {
    private final EmployeeDao employeeDao;
    @Autowired
       public EmployeeController(EmployeeDao employeeDao) {
        this.employeeDao=employeeDao;
    }

    @PostMapping("/employees")
    public void addEmployee(@RequestBody Employee employee) {
       employeeDao.addEmployee(employee);
    }


    @GetMapping("/employees/{emp_id}")
    public Employee getEmployeeByID(@PathVariable("emp_id") int emp_id) {

        return employeeDao.getEmployeeByID(emp_id).get();

    }

    @PutMapping("/employees/{emp_id}")
    public int updateEmployee(@RequestBody Employee employee,@PathVariable("emp_id") int emp_id) {
        return employeeDao.updateEmployee(emp_id,employee);
    }

    @GetMapping("/employees/delete/{emp_id}")
    public void deleteEmployee(@PathVariable("emp_id")int emp_id){
        employeeDao.deleteEmployee(emp_id);
    }

@PatchMapping("/employees/{emp_id}")
    public int updateOne(@RequestBody Employee employee,@PathVariable("emp_id") int emp_id){
        return employeeDao.updateOne(emp_id,employee);
}

}

