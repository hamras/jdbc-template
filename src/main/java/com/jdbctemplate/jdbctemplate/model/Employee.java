package com.jdbctemplate.jdbctemplate.model;


import javax.persistence.*;

@Entity
@Table(name = "emp_det")
public class Employee {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int emp_id;
    @Column
    private String emp_name;
    @Column
    private String emp_address;
    @Column
    private String emp_designation;
    @Column
    private String emp_contact;
    @Column(name = "emp_mail")
    private String emp_email;
    @Column
    private double emp_ctc;
    @Column
    private int emp_experience;
    @Column
    private String emp_bloodgroup;


    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getEmp_address() {
        return emp_address;
    }

    public void setEmp_address(String emp_address) {
        this.emp_address = emp_address;
    }

    public String getEmp_designation() {
        return emp_designation;
    }

    public void setEmp_designation(String emp_designation) {
        this.emp_designation = emp_designation;
    }

    public String getEmp_contact() {
        return emp_contact;
    }

    public void setEmp_contact(String emp_contact) {
        this.emp_contact = emp_contact;
    }

    public String getEmp_email() {
        return emp_email;
    }

    public void setEmp_email(String emp_email) {
        this.emp_email = emp_email;
    }

    public double getEmp_ctc() {
        return emp_ctc;
    }

    public void setEmp_ctc(double emp_ctc) {
        this.emp_ctc = emp_ctc;
    }

    public int getEmp_experience() {
        return emp_experience;
    }

    public void setEmp_experience(int emp_experience) {
        this.emp_experience = emp_experience;
    }

    public String getEmp_bloodgroup() {
        return emp_bloodgroup;
    }

    public void setEmp_bloodgroup(String emp_bloodgroup) {
        this.emp_bloodgroup = emp_bloodgroup;
    }

    @Override
    public String toString() {
        return "employee{" +
                "emp_id=" + emp_id +
                ", emp_name='" + emp_name + '\'' +
                ", emp_address='" + emp_address + '\'' +
                ", emp_designation='" + emp_designation + '\'' +
                ", emp_contactno=" + emp_contact +
                ", emp_email='" + emp_email + '\'' +
                ", emp_ctc=" + emp_ctc +
                ", emp_experience=" + emp_experience +
                ", emp_blodgroup='" + emp_bloodgroup + '\'' +
                '}';
    }
}

