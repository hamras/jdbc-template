package com.jdbctemplate.jdbctemplate.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<Employee> {


    @Override
    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {

        Employee emp=new Employee();
        emp.setEmp_id(rs.getInt(1));
        emp.setEmp_name(rs.getString(2));
        emp.setEmp_address(rs.getString(3));
        emp.setEmp_designation(rs.getString(4));
        emp.setEmp_email(rs.getString(5));
        emp.setEmp_ctc(rs.getDouble(6));
        emp.setEmp_experience(rs.getInt(7));
        emp.setEmp_bloodgroup(rs.getString(8));
        emp.setEmp_contact(rs.getString(9));

        return emp;

    }
}
