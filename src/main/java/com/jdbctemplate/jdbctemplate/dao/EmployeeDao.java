package com.jdbctemplate.jdbctemplate.dao;

import com.jdbctemplate.jdbctemplate.model.Employee;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface EmployeeDao {


   Optional<Employee> getEmployeeByID(int emp_id);
    int updateEmployee(int emp_id, Employee employee);
int updateOne(int emp_id,Employee employee);

    int addEmployee(Employee employee);
    int deleteEmployee(int emp_id);
}
