package com.jdbctemplate.jdbctemplate.dao;

import com.jdbctemplate.jdbctemplate.model.Employee;
import com.jdbctemplate.jdbctemplate.model.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public EmployeeDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int addEmployee(Employee employee) {
        String sql = "INSERT INTO emp_det VALUES(?,?,?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql,employee.getEmp_id(), employee.getEmp_name(), employee.getEmp_address(), employee.getEmp_designation(), employee.getEmp_email(), employee.getEmp_contact(), employee.getEmp_ctc(), employee.getEmp_experience(), employee.getEmp_bloodgroup());
    }


    @Override
    public Optional<Employee> getEmployeeByID(int emp_id) {
        String sql="SELECT * FROM emp_det WHERE emp_id=?;";
        return jdbcTemplate.query(sql,new UserRowMapper(),emp_id).stream().findFirst();
    }

    @Override
    public int updateEmployee(int emp_id, Employee employee) {
        String sql="UPDATE emp_det SET emp_id=?,emp_name=?,emp_address=?,emp_designation=?,emp_contact=?,emp_mail=?,emp_ctc=?,emp_experience=?,emp_bloodgroup=? WHERE emp_id=?;";
                return jdbcTemplate.update(sql,employee.getEmp_id(),employee.getEmp_name(),employee.getEmp_address(), employee.getEmp_designation(), employee.getEmp_contact(), employee.getEmp_email(), employee.getEmp_ctc(), employee.getEmp_experience(), employee.getEmp_bloodgroup(),emp_id);
    }

    @Override
    public int deleteEmployee(int emp_id) {
        String sql="DELETE FROM emp_det WHERE emp_id=?;";
        return jdbcTemplate.update(sql,emp_id);
    }

   @Override
    public int updateOne(int emp_id,Employee employee){
       String sql="UPDATE emp_det SET emp_id=?,emp_name=?,emp_address=?,emp_designation=?,emp_contact=?,emp_mail=?,emp_ctc=?,emp_experience=?,emp_bloodgroup=? WHERE emp_id=?;";
       return jdbcTemplate.update(sql,employee.getEmp_id(),employee.getEmp_name(),employee.getEmp_address(), employee.getEmp_designation(), employee.getEmp_contact(), employee.getEmp_email(), employee.getEmp_ctc(), employee.getEmp_experience(), employee.getEmp_bloodgroup(),emp_id);

   }


}




